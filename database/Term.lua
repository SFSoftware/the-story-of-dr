----------------------------------
-- Shadow RPG - Term
-- By Joshua
-- At 2013-01-29 09:03:21
----------------------------------
module(..., package.seeall)

GameName	= "郸日の完美青少年故事"
Welcome		= "--- The story of DanRi(a perfect teen story) ---"
Menu		= "系统菜单"
Start		= "开始游戏"
Save		= "储存进度"
Load		= "加载进度"
Maker		= "开发名单"
Exit		= "退出游戏"
Loaded		= "[欢迎回到这个故事。]"
SaveTip		= "[你确认要覆盖存档吗？]"
Registered	= "[欢迎来到这个故事——]\n[关于郸日，一个少年的故事。]"
ExitTip		= "[确认要退出 郸日の完美青少年故事 吗？]"
MakerList	= "[开发者名单]\n\n**策划 - Shuenhoy\n**开发 - Joshua & Shuenhoy\n**剧情 - Shuenhoy & FuHei Cat\n**出品 - SFS\n"
OK			= "确认"
Pass		= "取消"
GameOver	= "[郸日の完美青少年故事]\n[THE END]"
GameFin		= "[这个少年的故事已经结束。]"
GameFinSaveTip	= "[请保存 郸日の完美青少年故事 的通关存档。]"
SaveDataInfo	= "郸日の完美青少年故事 存档数据"

--- Config ---
SRDdir = "database"
SRDfn = "Main"
EnableWalkDir = true