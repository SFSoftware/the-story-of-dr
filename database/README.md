﻿# README #

这是一份关于Shadow RPG Data的使用及编辑说明文件。

### 语法 ###

#### Block ####
 
* 在解析时，SRF会读取入口SRD，然后首先替换掉所有的 `&include` 指令（和预编译类似），之后将以形如 #Main{ ... lines ... } 的内容（此处指 { *这部分* }）存入 PlotData.Main 中（EnableWalkDir的场合是PlotData["Main.Main"]）。

* 这里提供一个参数，EnableWalkDir，当它值为true时将会解析 `./SRDdir/` 下的所有SRD，每个SRD有独立命名空间；而值为false时则仅加载 `./{SRDdir}/{SRDfn}.srd` 。

* 一般来说，一份SRF文件形如这样：

```
#Main{
	...
}

#pre-story{
	...
}

...

#End

```

* 其中，`#(...){` 这部分的内容可以是任意字符串（其中不能含有 `\0` 和 `\n` 以及符号 `@` ，在启用EnableWalkDir的场合也请不要使用符号 `.` ，这不会造成Runing Error，但可能会出现预期外的错误）。其中，剧情跳转使用其作为标识符。如果有重复的标识符，后面的将会覆盖前面的。每次剧情选择（Select）都会给玩家带来一次存档机会（按M调出菜单），同时也可以调出退出选单（ESC）。因此请合理命名标识符来区分不同章节、支线。

* 建议在不同的Block间预留一个空行，在#End末尾回车添加空行。

* SRF会从 `#Main` 开始执行。

* 所有剧情文本都需要写在Block中。Block外的剧情文本不会作处理。

* Block的标识（即#...{和}）两行开头 **不允许任何缩进** 。其余文本允许任何形式的缩进。

* 在Main.srd的末尾请使用 `#End` 表示结束。

#### Text ####

* 在SRD中，每一行都是一个文本。以剧情解析符开头的行，将会作解析，以命令解析符开头的行则会执行相应指令。

* 文本中换行使用 \ ，在 Hint 中例外，使用 ]\[ 。

* 彩色文本使用 `[color text]`，也请善加利用。支持共16色，color分别为 `black` `blue` `green` `lgreen` `red` `purple` `yellow` `white` `gray` `lblue` `lgreen` `lsgreen` `lred` `lpurple` `lyellow` `bwhite`。

* 由于Windows Console编码问题，SRF的所有包含中文字符的文件均使用ANSI编码。因此，SRD文件也请务必使用ANSI。README和纯英文的文件例外，使用utf-8 without bom。

#### Include ####

* 形同预编译指令。

* 格式为 `&include:Target` ，将会读取database/Target.srd。注意，Target可以为 `a/b` 这样的形式，将会读取database/a/b.srd。

* 在读取Main时就会把这些指令全部替换。被替换的只是内容而已，因此如果不违反SRD的语法的话，在Block内部Include也是允许的（这样的话在Target里就不能出现Block定义，并且需要把EnableWalkDir改为false，统一使用Include指令。）。

#### Message ####

* 普通的信息。

* 格式形如 `>Message`，使用 \ 来换行。在不包含换行符的情况下将会居中显示；除此外是左对齐的（前有缩进），请合理利用\t及space来排版。

#### Hint ####

* 提示信息。

* 格式形如 `!Message`，会出现一个由#组成的方框（固定大小），你所写的文本将会居中放置。换行使用 `]\[` 。另外，Message中不允许出现英文半角方括号，请使用其它字符代替。

* 请多多测试是否超出文本框。如果超出不会产生错误，但影响视觉效果。

#### Select ####

* 选择分支。

* 格式形如 `$Message`，最适合放长文本，换行使用 \ 。

* 使用Select即宣告Block结束。下接 n 行选项，每行形如 `:Text@Target`，会出现名为Text的选项，选择后将跳转到 `Block#Target` 。

* 分支不存在时无法选择。

#### Note ####

* 开头是~的文本将被忽略，不被执行。事实上，不为剧情解析符或命令解析符开头的文本也会被忽略。

#### Swap ####

* 跳转Block。形如 `@Target`。在转到其它SRD时，使用形如 `@Chapt#0.Target` 的样式（开关是EnableWalkDir，一旦关闭就不能用这种形式，同时允许在BlockID中使用 `.` 符号）。

* 如果使用了 `&include` 指令，它会被载入两次（在启用了EnableWalkDir的场合）。你可以在 Main.srd 中跳转到目标SRD的对应Block，形如 `@Target` ，而不必使用 `@Target.Target` 这样的指令。在单SRD内的Swap只需使用 `@Target` 即可。

#### Command Var ####

* 用于操作变量。形如 `#var=1` ，等号左边是变量名，等号右边如需用到变量本身（或者其它变量）请使用 `{var}` 。不能储存table，但可以使用它执行一些Lua代码。请确保在执行运算操作时的所有元素都不为nil。（例如，在var不存在时，不要使用`#var={var}+1`，而应该使用`#var=({var} or 0)+1`。）

* 变量将被储存到sav文件中。

#### Command If ####

* 用于执行判断。形如 `?{var}>=30{ ... lines ... }`。

* 请注意不能写在单行中。

* 不存在Else或者Elseif，请另外使用一个If。

* 由于命令操作符判定为首个非空格字符，所以结束一个If后马上接另一个If是不允许的。（像是 } `?{var}==2` { 这样）

#### Commands ####

* 除了上述的命令之外，由&开头的是特殊的命令。

* 目前有 `&gameover` ，`&flash:c1,c2,c3` ，`&playbgm:bgm` ，`&stopbgm` ， `&playsound` ， `&playsound:sound` ， `&playvideo:video` ， `&showpicture:pic`这几个命令。

* 执行Commands时不必顾虑扩展名。

### 存放 ###

* 解析入口为 Main.srd ，在SRD中允许使用 &include 指令来加载其它的srd文件（可以存放在任意层子目录下，但扩展名必须为 *.srd）。

* 原则上命名为 Chapt#a.srd ，感觉写够多了（或者是需要存放独立的分支）就换个文件继续。 

* **`EnableWalkDir = false`的场合，所有的SRD共用同一个Block ID储存空间** ，所以请合理命名。

* `EnableWalkDir = true`的场合，不同的SRD，Block ID储存空间不同，因此不必顾虑。
