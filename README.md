# README #

It is the Project@SoD.

### Framework ###

* Use Shadow RPG Framework(lua).
* Plot write by SRD(Shadow RPG Data)

### Test ###

* You can run **Debug.bat** to check Running Error.

### Plot ###

* It is the Story of **DanRi**.

### Hint ###

* You can get the SRD Syntax highlighting Config (Shadow RPG Data.xml) and a theme (Black board.xml) there (for notepad++): [https://bitbucket.org/SFSoftware/the-story-of-dr/downloads](https://bitbucket.org/SFSoftware/the-story-of-dr/downloads)

### SRF Confiig ###

* `EnableWalkDir = true`