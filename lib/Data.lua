----------------------------------
-- Shadow RPG - Data
-- By Joshua
-- At 2013-01-27 11:57:42
----------------------------------
local table = table
local io = io
local os = os
local Term = require("database.Term")
local lfs = require("lfs")
module(..., package.seeall)
----------------------------------
-- Data - Create
-- By Joshua
-- At 2013-01-27 12:00:58
--
-- Updated By Joshua
-- At 2014-06-19 13:48:18
----------------------------------
function Create(Name, Nick)
	UserData = {
		Unit = Term.EnableWalkDir and "Main.Main" or "Main", 
		LastUnit = "",
		Now = 1, 
		Var = {
		},
	}
end
----------------------------------
-- Data - Clear
-- By Joshua
-- At 2013-06-22 22:42:19
----------------------------------
function Clear()
	UserData = nil
end
----------------------------------
-- Data - GameOver
-- By Joshua
-- At 2013-01-30 13:02:41
----------------------------------
function GameOver()
	UserData.Now = 0
end
----------------------------------
-- Data - GetExp
-- By Joshua
-- At 2013-05-25 20:53:20
----------------------------------
function GetExp(exp)
	for i, v in pairs(UserData.Var) do
		exp = exp:gsub(string.format("{%s}", i), tostring(v))
	end
	_, res = pcall(loadstring("return " .. exp))
	return res
end
----------------------------------
-- Data - SetVar
-- By Joshua
-- At 2013-05-25 20:53:20
----------------------------------
function SetVar(var, con)
	UserData.Var[var] = GetExp(con)
end
----------------------------------
-- Data - LoadData
-- By Joshua
-- At 2013-01-27 11:11:44
----------------------------------
function LoadData(ID)
	local a, b = pcall(dofile, "savedata/" .. ID .. ".sav")
	if type(b) == "table" then
		return b
	else
		io.write("=== Data Error ===\nMsg: ", b)
	end
end
----------------------------------
-- Data - Load
-- By Joshua
-- At 2013-01-29 21:02:17
----------------------------------
function Load(ID)
	UserData = LoadData(ID)
end
----------------------------------
-- Data - SaveLua
-- By Joshua
-- At 2013-01-27 11:12:01
----------------------------------
function SaveLua(k, v, n)
	local r = {}
	n = n or 0
	if type(v) ~= 'table' then
		table.insert(r, table.concat{('\t'):rep(n), '[', type(k) == 'string' and '"' .. k .. '"' or k, ']\t= ', type(v) == 'string' and '"' .. tostring(v) .. '"' or tostring(v):gsub("\"", "\\\""), ', \n'})
	else
		if n ~= 0 then
			table.insert(r, table.concat{('\t'):rep(n), '[', type(k) == 'string' and '"' .. k .. '"' or k, ']\t= {\n'})
		else
			table.insert(r, table.concat{k, "{\n"})
		end
		for i, t in pairs(v) do
			table.insert(r, SaveLua(i, t, n + 1))
		end
		if n ~= 0 then
			table.insert(r, table.concat{('\t'):rep(n), '}, \n'})
		else
			table.insert(r, table.concat{"}"})
		end
	end
	return table.concat(r)
end
----------------------------------
-- Data - Luac
-- By Joshua
-- At 2013-01-28 11:03:46
----------------------------------
function Luac(ID)
	os.execute(("Bin\\luac.exe -o savedata\\%s.sav savedata\\%s.sav"):format(ID, ID))
end
----------------------------------
-- Data - SaveData
-- By Joshua
-- At 2013-01-27 11:12:28
-- 
-- Updated By Joshua
-- At 2014-06-19 13:51:08
----------------------------------
function SaveData(ID)
	local file = io.open("savedata/" .. ID .. ".sav", "w")
	UserData.LastUnit = UserData.Unit
	file:write(SaveLua("return ", UserData))
	file:close()
end
----------------------------------
-- Data - Assert
-- By Joshua
-- At 2013-01-27 16:23:31
----------------------------------
function Assert()
	if not UserData then 
		return true
	end
end
----------------------------------
-- Data - GetData
-- By Joshua
-- At 2013-01-27 15:41:38
----------------------------------
function GetData(ID)
	local t = LoadData(ID)
	if type(t) == "table" and t.Now and t.Unit and t.Var then
		if t.Now == 0 then
			return "<Game Over>" .. Term.SaveDataInfo
		else
			return Term.SaveDataInfo
		end
	else
		return "Data Error"
	end
end
----------------------------------
-- Data - GetUnit
-- By Joshua
-- At 2013-02-01 19:25:26
----------------------------------
function GetUnit()
	return UserData.Unit
end
----------------------------------
-- Data - GetLastUnit
-- By Joshua
-- At 2014-06-19 13:52:05
----------------------------------
function GetLastUnit()
	return UserData.LastUnit
end
----------------------------------
-- Data - SetUnit
-- By Joshua
-- At 2013-02-01 19:25:26
----------------------------------
function SetUnit(u)
	UserData.Unit = u
end
----------------------------------
-- Data - LoadPlot
-- By Joshua
-- At 2013-05-16 19:17:02
--
-- Updated By Shuenhoy
-- At 2014-06-16 18:37:50
-- 
-- Updated By Joshua
-- At 2014-06-17 19:15:24
--
-- Updated By Shuenhoy
-- At 2014-06-18 06:01:56
--
-- Updated By Joshua
-- At 2014-06-20 13:28:27
----------------------------------
function LoadPlot(file, db)
	local f = io.open(file, "r")
	local t = f:read("*a")
	f:close()
	db = db or {}
	for fn in t:gmatch("\n&include:([^\n]-)[ \t]*\n") do
		local f = io.open(Term.SRDdir.."\\" .. fn .. ".srd", "r")
		local tLocal = f:read("*a")
		f:close()
		t = t:gsub("&include:" .. fn .. "[ \t]*", tLocal)
	end
	for unit, text in t:gmatch("#([^\n]-){%s*(.-)\n}[ \t]*\n") do
		if Term.EnableWalkDir then
			unit = file:match("(.-)%.srd$"):gsub("\\", "."):gsub(Term.SRDdir.."%.", "") .. "." .. unit
		end
		text = text .. "\n"
		db[unit] = {}
		for line in text:gmatch("%s*([^\n]-)\n") do
			if line:sub(1, 1) == "\\" and #db[unit] > 0 then
				db[unit][#db[unit]] = db[unit][#db[unit]] .. line
			else
				table.insert(db[unit], line)
			end
		end
	end
	return db
end
----------------------------------
-- Data - walkDir
-- By Shuenhoy
-- At 2014-06-16 18:19:39
----------------------------------
local function walkDir (path, db)
	path = path .. "\\" 
    for file in lfs.dir(path) do 
        if file ~= "." and file ~= ".." then 
            local f = path..file 
            if string.find(f, "%.srd$") ~= nil then 
            	print("!",path:match("(.-)"))
                LoadPlot(f, db)
            end 
            local attr = lfs.attributes(f) 
            assert (type(attr) == "table") 
            if attr.mode == "directory" then 
                walkDir (f, db) 
            end 
        end 
    end
end
----------------------------------
-- Data - LoadPlots
-- By Shuenhoy
-- At 2014-06-16 19:44:31
--
-- Updated By Joshua
-- At 2014-06-19 13:25:05
----------------------------------
function LoadPlots()
	local db = {}
	if Term.EnableWalkDir then
		walkDir(Term.SRDdir, db)
	else
		return LoadPlot(Term.SRDdir .. "/" .. Term.SRDfn .. ".srd", db)
	end
	return db
end