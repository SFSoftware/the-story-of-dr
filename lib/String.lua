----------------------------------
-- Shadow RPG - String
-- By Joshua
-- At 2013-01-28 17:20:40
----------------------------------
local string = string

module(..., package.seeall)
----------------------------------
-- String - QFind
-- By Joshua
-- At 2013-01-11 13:28:58
----------------------------------
function QFind(str, fstr)
	if fstr:find("|.-|") then
		local r = {}
		local s = fstr:match("|(.-)|")
		if s == "" then
			return str:find(fstr)
		end
		s = s .. ", "
		for i in s:gmatch("(.-), ") do
			r[#r + 1] = i
		end
		for i, v in pairs(r) do
			if QFind(str, fstr:gsub("|.-|", v, 1)) then
				return true
			end
		end
	else
		return str:find(fstr)
	end
end
----------------------------------
-- String - GetLength
-- By Joshua
-- At 2013-01-28 18:57:10
--
-- Updated By Joshua 
-- At 2014-06-19 21:55:45
----------------------------------
function GetLength(str)
	for i, v in pairs(io.Colors) do
		str = str:gsub("%[" .. i .. " (.-)%]", "%1")
	end
	return #str
end