----------------------------------
-- Shadow RPG - Audio
-- By Shuenhoy
-- At 2013-01-28 11:28:50
----------------------------------
local alien = require "alien"
local Winmm  =  alien.load("winmm")
local User32 =  alien.load("user32")
Winmm.PlaySound:types{"string", "int", "int"}
Winmm.mciSendStringA:types{"string", "string", "int", "pointer"}
User32.MessageBeep:types{"long"}


module(..., package.seeall)

flags = {
	sync = 0, 
	async = 1, 
	loop = 8, 
}

value = {
	ok = 0, 
	hand = 16, 
	question = 32, 
	tip = 48, 
	error = 64
}
----------------------------------
-- Audio - sendMMI
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
local function sendMMI(str)
	Winmm.mciSendStringA(str, nil, 0, 0)
end
----------------------------------
-- Audio - LoadMusic
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function LoadMusic(filename)
	sendMMI("open " .. filename)
end
----------------------------------
-- Audio - PlayMusic
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function PlayMusic(filename)
	LoadMusic(filename)
	sendMMI("play "..filename.." repeat")
end
----------------------------------
-- Audio - PlaySound
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function PlaySound(filename,re)
	LoadMusic(filename)
	sendMMI("play "..filename)
end
----------------------------------
-- Audio - Pause
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function PauseMusic(filename)
	sendMMI("pause " .. filename)
end
----------------------------------
-- Audio - ResumeMusic
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function ResumeMusic(filename)
	sendMMI("resume " .. filename)
end
----------------------------------
-- Audio - StopMusic
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function StopMusic(filename)
	sendMMI("stop " .. filename)
end
----------------------------------
-- Audio - CloseMusic
-- By Shuenhoy
-- At 2014-06-17 20:34:44
----------------------------------
function CloseMusic(filename)
	sendMMI("close " .. filename)
end
----------------------------------
-- Audio - Play
-- By Shuenhoy
-- At 2013-01-28 11:29:16
----------------------------------
function Play(filename, flag)
	Winmm.PlaySound(filename, 0, flag or flags.async)
end
----------------------------------
-- Audio - Stop
-- By Shuenhoy
-- At 2013-01-28 11:29:38
----------------------------------
function Stop()
	Winmm.PlaySound("", 0, 0)
end
----------------------------------
-- Audio - Beep
-- By Joshua
-- At 2013-01-28 11:40:31
----------------------------------
function Beep(v)
	if type(v) == "string" then
		return User32.MessageBeep(value[v] or 0)
	else
		return User32.MessageBeep(v or 0)
	end
end
