----------------------------------
-- Shadow RPG - System
-- By Joshua
-- At 2013-01-26 20:08:41
----------------------------------
local io = io
local os = os
local table = table
local lcon = lcon
local Audio = require("lib.Audio")
local Util = require("lib.Util")
local Data = require("lib.Data")
local String = require("lib.String")
local lfs = require("lfs")
local socket = require("socket")
local Term = require("database.Term")
local sfstr = "Save%.3d"

module(..., package.seeall)
----------------------------------
-- System - Main
-- By Joshua
-- At 2013-01-27 11:03:48
----------------------------------
function Main(back)
	PlotData = Data.LoadPlots()

	Util.Cls()
	local res = Print({"\t     " .. Term.Welcome .. "\n\n\t\t=== " .. Term.Menu .. " ===", 
		{Term.Start}, 
		{Term.Save, Data.Assert() and "G"}, 
		{Term.Load}, 
		{Term.Maker}, 
		{Term.Exit}
	})
	if res == 1 then
		Start()
	elseif res == 2 then
		Save()
	elseif res == 3 then
		Load()
	elseif res == 4 then
		Maker()
	elseif res == 5 then
		Exit()
	end
	if not back then
		return Main()
	end
end
----------------------------------
-- System - Save
-- By Joshua
-- At 2013-01-27 15:22:35
----------------------------------
function Save(Back)
	local Out={"\t\t  === " .. Term.Save .. " ===\n"}
	local saved = {}
	for i = 1, 10 do
		if lfs.touch("savedata\\" .. sfstr:format(i) .. ".sav") then
			saved[i] = true
			table.insert(Out, {sfstr:format(i) .. " - " .. Data.GetData(sfstr:format(i))})
		else
			table.insert(Out, {sfstr:format(i) .. " - No Data"})
		end
	end
	local res = Print(Out, 2)
	if not saved[res] or WantSave() then
		NowSave(sfstr:format(res))
	end
end
----------------------------------
-- System - Start
-- By Joshua
-- At 2013-01-28 10:14:40
-- 
-- Updated By Joshua
-- At 2014-06-19 13:52:51
----------------------------------
function Start()
	if Data.Assert() then
		Register()
	end
	local u = Data.GetUnit()
	if not PlotData[u] then
		io.write("\n\t[lred Error: No Plot Data], [lyellow Data: Block ID - " .. u .. "]")
		Util.WaitAnyKey()
		Data.SetUnit(Term.SRDfn..".Main")
		return Main()
	end
	local Out = {}
	local OutMode = false
	local IfMode = false
	local exp
	for i = 1, #PlotData[u] do
		local ch = PlotData[u][i]:sub(1, 1)
		local rstr = PlotData[u][i]:sub(2, -1):gsub("\\", "\n")
		local IfTrue
		if IfMode then
			IfTrue = Data.GetExp(exp)
		end
		if OutMode and ch == ":" and (not IfMode or IfTrue) then
			local warp = PlotData[u][i]:match("@(.-)%s*$")
			local text = PlotData[u][i]:match(":(.-)@")
			if Term.EnableWalkDir and not warp:match("%.[^%.]+$") then
				warp = Data.GetUnit():gsub("%.[^%.]+$","." .. warp)
			end
			table.insert(Out, {text, not PlotData[warp] and "G", warp})
		else
			if not IfMode or IfTrue then
				if ch == ">" then
					PutMsg(rstr:find("\n") and rstr or "[" .. rstr .. "]")
				elseif ch == "!" then
					MsgBox("[" .. rstr:gsub("\n", "]\n[") .. "]")
				elseif ch == "$" then
					OutMode = true
					Out[1] = rstr
				elseif ch == "#" then
					if Data.GetLastUnit() == Data.GetUnit() then
						local var = PlotData[u][i]:match("#(%S+)=")
						local con = PlotData[u][i]:match("=(.+)")
						Data.SetVar(var, con)
					end
				elseif ch == "&" then
					if PlotData[u][i]:match("&gameover") then
						GameFin()
					elseif PlotData[u][i]:match("&flash") then
						Util.Flash(PlotData[u][i]:match("&flash:(%S-),(%S-),(%S+)"))
					elseif PlotData[u][i]:match("&playbgm") then
						Audio.PlayMusic(".\\audio\\" .. PlotData[u][i]:match("&playbgm:(%S+)") .. ".mp3")
					elseif PlotData[u][i]:match("&stopbgm") then	
						Audio.StopMusic(".\\audio\\" .. PlotData[u][i]:match("&stopbgm:(%S+)") .. ".mp3")
					elseif PlotData[u][i]:match("&resumebgm") then	
						Audio.ResumeMusic(".\\audio\\" .. PlotData[u][i]:match("&resumebgm:(%S+)") .. ".mp3")
					elseif PlotData[u][i]:match("&pausebgm") then	
						Audio.PauseMusic(".\\audio\\" .. PlotData[u][i]:match("&pausebgm:(%S+)") .. ".mp3")
					elseif PlotData[u][i]:match("&playsound") then
						Audio.PlaySound(".\\audio\\" .. PlotData[u][i]:match("&playsound:(%S+)") .. ".mp3")
					elseif PlotData[u][i]:match("&playvideo") then	
						Util.PlayVideo(PlotData[u][i]:match("&playvideo:(%S+)"))
					elseif PlotData[u][i]:match("&showpicture") then	
						Util.ShowPicture(PlotData[u][i]:match("&showpicture:(%S+)"))
					end
				end
			end
			if ch == "?" then
				exp = PlotData[u][i]:match("?(.+){")
				IfMode = true
				IfTrue = false
			elseif ch == "}" and IfMode then
				IfMode = false
				IfTrue = false
				exp = nil
			elseif ch == "@" then
				local warp = PlotData[u][i]:match("@(%S+)")
				if Term.EnableWalkDir and not warp:match("%.[^%.]+$") then
					warp = Data.GetUnit():gsub("%.[^%.]+$","." .. warp)
				end
				Data.SetUnit(warp)
				return Start()
			end
		end
	end
	if OutMode then
		local res = Print(Out)
		Data.SetUnit(Out[1 + res][3])
		return Start()
	end
end
----------------------------------
-- System - Load
-- By Joshua
-- At 2013-01-27 19:25:22
----------------------------------
function Load(Back)
	local Out = {"\t\t  === " .. Term.Load .. " ===\n"}
	local CanLoad = {}
	for i = 1, 10 do
		if lfs.touch("savedata\\" .. sfstr:format(i) .. ".sav") then
			local Res = Data.GetData(sfstr:format(i))
			if Res == "Data Error" then
				table.insert(Out, {sfstr:format(i) .. " - " .. Res, "E"})
			elseif Res:sub(1, 11) == "<Game Over>" then
				table.insert(Out, {sfstr:format(i) .. " - " .. Res, "O"})
			else
				table.insert(Out, {sfstr:format(i) .. " - " .. Res})
				CanLoad[i] = true
			end
		else
			table.insert(Out, {sfstr:format(i) .. " - No Data", "G"})
		end
	end
	local res = Print(Out, 2)
	if CanLoad[res] then
		Data.Load(sfstr:format(res))
	end
	return Start()
end
----------------------------------
-- System - LoadData
-- By Joshua
-- At 2013-01-28 10:06:00
----------------------------------
function LoadData(ID)
	Data.LoadData(ID)
	MsgBox(Term.Loaded)
	return Start()
end
----------------------------------
-- System - CanSave
-- By Joshua
-- At 2013-01-27 17:58:08
----------------------------------
function WantSave(ID)
	return SelectBox(Term.SaveTip)
end
----------------------------------
-- System - NowSave
-- By Joshua
-- At 2013-01-27 17:59:03
----------------------------------
function NowSave(ID)
	if Data.Assert() then
		Register()
	end
	Data.SaveData(ID)
	Data.Luac(ID)
end
----------------------------------
-- System - Register
-- By Joshua
-- At 2013-01-27 18:13:35
----------------------------------
function Register()
	Util.Cls()
	io.SetColor()
	Data.Create(Name, Nick)
	MsgBox(Term.Registered)
end
----------------------------------
-- System - Maker
-- By Joshua
-- At 2013-01-27 11:04:03
----------------------------------
function Maker(Back)
	Util.Cls()
	return MsgBox(Term.MakerList)
end
----------------------------------
-- System - Exit
-- By Joshua
-- At 2013-01-27 11:04:10
----------------------------------
function Exit()    
	local res = SelectBox(Term.ExitTip)
	if res then
		return os.exit()
	end
end
----------------------------------
-- System - Print
-- By Joshua
-- At 2013-01-27 11:04:20
----------------------------------
function Print(Out, Tab)
	Tab = Tab or 1
	Util.Cls()
	io.SetColor()
	io.write(Out[1], "\n")
	io.write("=============================================================\n")
	Util.RSColor(Out[2][2], "lsgreen")
	io.write(("\t"):rep(Tab), "> ", Out[2][1], "\n")
	io.SetColor()
	for i=3, #Out do
		Util.RSColor(Out[i][2])
		io.write(("\t"):rep(Tab), "  ", Out[i][1], "\n")
	end
	io.SetColor()
	io.write("=============================================================\n")
	local m = #Out
	local x = 8 * Tab
	local y = lcon.cur_y() - m
	local point = 1
	while true do
		local Key = lcon.getch()
		if Key == 27 then
			Exit()
			return Print(Out, Tab)
		elseif Key == 72 and point > 1 then
			lcon.gotoXY(x, y)
			Util.RSColor(Out[point + 1][2])
			io.write("  ", Out[point + 1][1])
			io.SetColor()
			point = point - 1
			y = y - 1
			lcon.gotoXY(x, y)
			if Util.RSColor(Out[point + 1][2]) then
				io.SetColor("lsgreen")
			end
			io.write("> ", Out[point + 1][1])
			io.SetColor()
		elseif Key == 80 and point < m - 1 then
			lcon.gotoXY(x, y)
			Util.RSColor(Out[point + 1][2])
			io.write("  ", Out[point + 1][1])
			point = point + 1
			y = y + 1
			lcon.gotoXY(x, y)
			if Util.RSColor(Out[point + 1][2]) then
				io.SetColor("lsgreen")
			end
			io.write("> ", Out[point + 1][1])
			io.SetColor()
		elseif Key == 13 then
			io.SetColor()
			if not Util.RSColor(Out[point + 1][2]) then
				Audio.Beep("tip")
				socket.sleep(0.1)
				lcon.gotoXY(x, y)
				io.write(">  ", Out[point + 1][1])
				socket.sleep(0.1)
				lcon.gotoXY(x, y)
				io.write("> ", Out[point + 1][1], "  ")
				io.SetColor()
			else
				return point
			end
		elseif Key == 109 then
			Main(true)
			return Print(Out, Tab)
		end
	end
end
----------------------------------
-- System - MsgBox
-- By Joshua
-- At 2013-01-27 11:04:30
----------------------------------
function MsgBox(Out)
	Util.Cls()
	io.SetColor()
	local Max = 47
	local res = {}
	Out = Out .. "\n"
	while Out and Out:find("\n") do
		local r = Out:match("^(.-)\n")
		if r:find("^%[.-%]$") then
			table.insert(res, r:match("^%[(.-)%]$"))
		else
			local r = r:gsub("%*", "    ")
			table.insert(res, r .. (" "):rep(Max - String.GetLength(r)))
		end
		Out = Out:match("^.-\n(.+)$")
	end
	io.write("\t=================================================\n")
	for i, v in pairs(res) do
		v = (" "):rep(math.floor((Max - String.GetLength(v)) / 2)) .. v
		io.write("\t#", v .. (" "):rep(Max - String.GetLength(v)), "#\n")
	end
	io.write("\t#\t\t\t\t\t\t#\n")
	io.write("\t#\t\t     ")
	io.write("[lsgreen > " .. Term.OK .. "]")
	io.write("\t\t\t#\n")
	io.write("\t=================================================\n")
	local key = 0
	while key ~= 13 do
		key = lcon.getch()
	end
end
----------------------------------
-- System - PutMsg
-- By Joshua
-- At 2013-01-27 11:04:30
----------------------------------
function PutMsg(Out)
	Util.Cls()
	io.SetColor()
	local Max = 47
	local res = {}
	Out = Out .. "\n"
	while Out and Out:find("\n") do
		local r = Out:match("^(.-)\n")
		if r:find("^%[.-%]$") then
			table.insert(res, r:match("^%[(.-)%]$"))
		else
			local r = r:gsub("%*", "    ")
			table.insert(res, r .. (" "):rep(Max - String.GetLength(r)))
		end
		Out = Out:match("^.-\n(.+)$")
	end
	io.write("\n\n\n\n\n")
	for i, v in pairs(res) do
		v = (" "):rep(math.floor((Max - String.GetLength(v)) / 2)) .. v
		io.write("\t ", v .. (" "):rep(Max-String.GetLength(v)), "\n")
	end
	io.write("\t\t\t\t\t\t\t\n")
	io.write("\t\t\t\t[lsgreen ��]")
	local key = 0
	while key ~= 13 do
		key = lcon.getch()
	end
end
----------------------------------
-- System - InputBox
-- By Joshua
-- At 2013-01-27 18:30:29
----------------------------------
function InputBox(Out, Tip)
	Util.Cls()
	io.SetColor()
	local Max = 47
	local res = {}
	Out = Out .. "\n"
	while Out and Out:find("\n") do
		local r = Out:match("^(.-)\n")
		if r:find("^%[.-%]$") then
			table.insert(res, r:match("^%[(.-)%]$"))
		else
			local r = r:gsub("%*", "    ")
			table.insert(res, r .. (" "):rep(Max - String.GetLength(r)))
		end
		Out = Out:match("^.-\n(.+)$")
	end
	io.write("\t=================================================\n")
	for i, v in pairs(res) do
		v = (" "):rep(math.floor((Max-String.GetLength(v)) / 2)) .. v
		io.write("\t#", v .. (" "):rep(Max-String.GetLength(v)), "#\n")
	end
	io.write("\t#\t\t\t\t\t\t#\n")
	io.write("\t#\t\t\t\t\t\t#\n")
	io.write("\t=================================================\n")
	lcon.gotoXY(12, lcon.cur_y()-2)
	if Tip then
		io.write(Tip) 
	end
	return io.read(30)
end
----------------------------------
-- System - SelectBox
-- By Joshua
-- At 2013-01-27 11:04:37
----------------------------------
function SelectBox(Out)
	Util.Cls()
	io.SetColor()
	local Max = 47
	local res = {}
	Out = Out .. "\n"
	while Out and Out:find("\n") do
		local r = Out:match("^(.-)\n")
		if r:find("^%[.-%]$") then
			table.insert(res, r:match("^%[(.-)%]$"))
		else
			local r = r:gsub("%*", "    ")
			table.insert(res, r .. (" "):rep(Max-String.GetLength(r)))
		end
		Out = Out:match("^.-\n(.+)$")
	end
	io.write("\t=================================================\n")
	for i, v in pairs(res) do
		v = (" "):rep(math.floor((Max-String.GetLength(v)) / 2)) .. v
		io.write("\t#", v .. (" "):rep(Max-String.GetLength(v)), "#\n")
	end
	io.write("\t#\t\t\t\t\t\t#\n")
	io.write("\t#\t\t ")
	io.write("[lsgreen > " .. Term.OK .. "]")
	io.write("      " .. Term.Pass .. "\t\t#\n")
	io.write("\t=================================================\n")
	local mouse = true
	while true do
		local key = lcon.getch()
		if key == 75 then
			lcon.gotoXY(35, 3)
			io.write("  " .. Term.Pass)
			lcon.gotoXY(25, 3)
			io.write("[lsgreen > " .. Term.OK .. "]")
			mouse = true
		elseif key == 77 or key == 27 then
			lcon.gotoXY(25, 3)
			io.write("  " .. Term.OK)
			lcon.gotoXY(35, 3)
			io.write("[lsgreen > " .. Term.Pass .. "]")
			mouse = false
		elseif key == 13 then
			io.SetColor()
			return mouse
		end
	end
end
----------------------------------
-- System - GameOver
-- By Joshua
-- At 2013-01-30 11:28:27
----------------------------------
function GameOver()
	MsgBox(Term.GameOver)
	Data.Clear()
end
----------------------------------
-- System - GameFin
-- By Joshua
-- At 2014-06-20 20:17:58
----------------------------------
function GameFin()
	MsgBox(Term.GameFin)
	MsgBox(Term.GameFinSaveTip)
	Data.GameOver()
	Save()
	Data.Clear()
end