----------------------------------
-- Shadow RPG - Util
-- By Joshua
-- At 2013-01-26 20:30:18
----------------------------------
local io = io
local os = os
local lcon = lcon
local Term = require("database.Term")
local socket = require("socket")
local lcps = require("lcps")

module(..., package.seeall)
----------------------------------
-- Util - SetTitle
-- By Joshua
-- At 2013-01-27 11:05:09
----------------------------------
function SetTitle()
	os.execute("title " .. Term.GameName)
end
----------------------------------
-- Util - RSColor
-- By Joshua
-- At 2013-01-30 11:39:44
----------------------------------
function RSColor(Mode, More)
	Mode = tostring(Mode)
	if Mode == "G" then
		io.SetColor("gray")
	elseif Mode == "O" then
		io.SetColor("green")
	elseif Mode == "E" then
		io.SetColor("red")
	else
		io.SetColor(More or "bwhite")
		return true
	end
end
----------------------------------
-- Util - Cls
-- By Joshua
-- At 2013-01-27 11:05:19
----------------------------------
function Cls()
	os.execute("cls")
end
----------------------------------
-- Util - Flash
-- By Joshua
-- At 2013-06-23 11:09:55
----------------------------------
function Flash(c1, c2, c3, t)
	assert(c1 and c2)
	c3 = c3 or c1
	t = t or 0.3
	c1 = CheckColors(c1)
	c2 = CheckColors(c2)
	c3 = CheckColors(c3)
	lcon.cls_c(c1, c1)
	socket.sleep(t)
	lcon.cls_c(c2, c2)
	socket.sleep(t)
	lcon.cls_c(c1, c1)
	socket.sleep(t)
	lcon.cls_c(c2, c2)
	socket.sleep(t)
	lcon.cls_c(c3, c3)
	return true
end
----------------------------------
-- Util - SetInfo
-- By Joshua
-- At 2013-06-23 10:59:00
----------------------------------
function SetInfo()
	lcon.hide_cursor()
end
----------------------------------
-- Util - ShowPicture
-- By Joshua
-- At 2013-06-29 22:21:16
----------------------------------
function ShowPicture(pn)
	lcps.pbmp(".\\image\\" .. pn .. ".bmp", nil, true)
end
----------------------------------
-- Util - PlayVideo
-- By Joshua
-- At 2013-06-29 22:22:07
----------------------------------
function PlayVideo(vn)
	lcps.pvd(".\\video\\" .. vn, false)
end
----------------------------------
-- Util - WaitAnyKey
-- By Joshua
-- At 2014-06-18 13:43:36
----------------------------------
function WaitAnyKey(vn)
	lcon.getch()
end
----------------------------------
-- Util - CheckColors
-- By Joshua
-- At 2014-06-20 13:30:03
----------------------------------
function CheckColors(n)
	return io.Colors[n] or n
end
