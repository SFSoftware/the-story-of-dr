# README #

这里提供编辑SRD所需的相关工具和为Notepad++编辑Lua代码所准备的主题及语言高亮配置，建议搭配使用。

其中，Shadow RPG Data.xml 会随着SRF代码的更新而更新。在Npp的语言 - 自定义语言格式内导入，在编辑SRD时在语言内选择 Shadow PRG Data 即可。